from click.testing import CliRunner

from cryptik import main


def test_version():
    runner = CliRunner()
    result = runner.invoke(main, ["--version"])
    # logger.info(f"RESULT IS=[{result.output}]")
    assert result.exit_code == 0  # nosec
    # assert result.output == 'cryptik - v.2.2.0 (2020-02-18)'.encode()


def test_default():
    runner = CliRunner()
    result = runner.invoke(main)
    assert result.exit_code == 0  # nosec


def test_invalid_exchange():
    runner = CliRunner()
    result = runner.invoke(main, ["--exchange", "nadda"])
    assert result.exit_code == 21  # nosec


def test_exchange_bitstamp():
    runner = CliRunner()
    result = runner.invoke(main, ["--exchange", "BITSTAMP", "--crypto-currency", "BTC"])
    # ~ logger.info(f"RESULT IS=[{result.output}]")
    assert result.exit_code == 0  # nosec


# ~ def test_exchange_btce_btc():
# ~ runner = CliRunner()
# ~ result = runner.invoke(main, ['--exchange', 'BTCE:BTC'])
# ~ assert result.exit_code == 0


# ~ def test_exchange_btce_ltc():
# ~ runner = CliRunner()
# ~ result = runner.invoke(main, ['--exchange', 'BTCE:LTC'])
# ~ assert result.exit_code == 0


def test_exchange_kraken_btc():
    runner = CliRunner()
    result = runner.invoke(main, ["--exchange", "KRAKEN", "--crypto-currency", "BTC"])
    assert result.exit_code == 0  # nosec


def test_exchange_kraken_ltc():
    runner = CliRunner()
    result = runner.invoke(main, ["--exchange", "KRAKEN", "--crypto-currency", "LTC"])
    assert result.exit_code == 0  # nosec


# ~ def test_exchange_kraken_nmc():
# ~ runner = CliRunner()
# ~ result = runner.invoke(main, ['--exchange', 'KRAKEN', '--crypto-currency', 'NMC'])
# ~ assert result.exit_code == 0

if __name__ == "__main__":
    test_default()
